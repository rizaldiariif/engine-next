import "bootstrap/dist/css/bootstrap.min.css";
import Header from "../src/components/Header";

import { ReactQueryCacheProvider, QueryCache } from "react-query";
import { Hydrate } from "react-query/hydration";

const queryCache = new QueryCache();

export default function MyApp({ Component, pageProps }) {
  return (
    <ReactQueryCacheProvider queryCache={queryCache}>
      <Hydrate state={pageProps.dehydratedState}>
        <Header />
        <Component {...pageProps} />
      </Hydrate>
    </ReactQueryCacheProvider>
  );
}
