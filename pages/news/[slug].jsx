import Axios from "axios";
import Head from "next/head";
import Image from "next/image";
import { QueryCache, useQuery } from "react-query";
import { dehydrate } from "react-query/hydration";
import { Container } from "reactstrap";

export default function Detail({ params, locale }) {
  const { data } = useQuery(
    [
      "news_detail",
      `${process.env.NEXT_PUBLIC_API_URL}/api/news?slug=${params.slug}&lang=${locale}`,
    ],
    prefetchAxios
  );
  const { data: dataContent } = useQuery(
    [
      "content-news-detail",
      `${process.env.NEXT_PUBLIC_API_URL}/api/contents?keys=favicon&lang=${locale}`,
    ],
    prefetchAxios
  );

  return (
    <Container className="py-4">
      <Head>
        {/* Primary Meta Tags */}
        <title>{data.data[0].title}</title>
        <meta
          name="description"
          content={data.data[0].content}
          key="description"
        />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" key="og:type" />
        <meta
          property="og:title"
          content={`${data.data[0].title} | COMPRO`}
          key="og:title"
        />
        <meta
          name="og:description"
          content={data.data[0].content}
          key="og:description"
        />
        <meta
          property="og:image"
          content={data.data[0].thumbnail}
          key="og:image"
        />

        {/* Twitter */}
        <meta
          property="twitter:card"
          content="summary_large_image"
          key="twitter:card"
        />
        <meta
          property="twitter:title"
          content={`${data.data[0].title} | COMPRO`}
          key="twitter:title"
        />
        <meta
          name="twitter:description"
          content={data.data[0].content}
          key="twitter:description"
        />
        <meta
          property="twitter:image"
          content={data.data[0].thumbnail}
          key="twitter:image"
        />

        {/* Favicon */}
        <link
          rel="icon"
          type="image/png"
          href={dataContent.data.favicon.value}
        />
      </Head>
      <h1 className="display-4">{data.data[0].title}</h1>
      <Image src={data.data[0].thumbnail} width="1600" height="900" />
      <div
        className="lead"
        dangerouslySetInnerHTML={{
          __html: data.data[0].content,
        }}
      ></div>
    </Container>
  );
}

async function prefetchAxios(key, url) {
  const result = await Axios.get(url);
  return result.data;
}

export async function getStaticPaths() {
  const res = await fetch(
    `${process.env.NEXT_PUBLIC_API_URL}/api/get_dynamic_routes?model=Article`
  );
  const data = await res.json();

  let paths = data.map((d, i) => {
    return [
      {
        params: {
          slug: d.translations[0].slug,
        },
        locale: d.translations[0].locale,
      },
      {
        params: {
          slug: d.translations[1].slug,
        },
        locale: d.translations[1].locale,
      },
    ];
  });

  paths = paths.flat();

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params, locale }) {
  const queryCache = new QueryCache();

  await queryCache.prefetchQuery(
    [
      "news_detail",
      `${process.env.NEXT_PUBLIC_API_URL}/api/news?slug=${params.slug}&lang=${locale}`,
    ],
    prefetchAxios
  );

  await queryCache.prefetchQuery(
    [
      "content-news-detail",
      `${process.env.NEXT_PUBLIC_API_URL}/api/contents?keys=favicon&lang=${locale}`,
    ],
    prefetchAxios
  );

  return {
    props: { dehydratedState: dehydrate(queryCache), params, locale },
    revalidate: 60,
  };
}
