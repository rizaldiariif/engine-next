import Link from "next/link";
import Head from "next/head";
import Image from "next/image";
import { QueryCache, useQuery } from "react-query";
import { dehydrate } from "react-query/hydration";
import { Container, Row, Col, Card, CardBody, Button } from "reactstrap";
import axios from "axios";

export default function Index({ query, locale }) {
  let news_link = `${process.env.NEXT_PUBLIC_API_URL}/api/news?perPage=10&lang=${locale}`;
  if (query.id_category) {
    news_link += `&article_category_id=${query.id_category}`;
  } else {
    query.id_category = "";
  }
  if (query.page) {
    news_link += `&page=${query.page}`;
  }

  const { data: dataNews } = useQuery(["news", news_link], prefetchAxios);
  const { data: news, meta } = dataNews;

  const { data: dataNewsCategory } = useQuery(
    [
      "news_categories",
      `${process.env.NEXT_PUBLIC_API_URL}/api/news_categories`,
    ],
    prefetchAxios
  );
  const { data: news_categories } = dataNewsCategory;
  const { data: dataContent } = useQuery(
    [
      "content-news-index",
      `${process.env.NEXT_PUBLIC_API_URL}/api/contents?keys=favicon,seo_title,seo_description,news_index_title,news_category_title,news_index_cta&lang=${locale}`,
    ],
    prefetchAxios
  );

  return (
    <Container className="py-4">
      <Head>
        {/* Primary Meta Tags */}
        <title>{dataContent.data.seo_title.value}</title>
        <meta
          name="description"
          content={dataContent.data.seo_description.value}
          key="description"
        />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" key="og:type" />
        <meta
          property="og:title"
          content={dataContent.data.seo_title.value}
          key="og:title"
        />
        <meta
          property="og:description"
          content={dataContent.data.seo_description.value}
          key="og:description"
        />
        <meta
          property="og:image"
          content={dataContent.data.favicon.value}
          key="og:image"
        />

        {/* Twitter */}
        <meta
          property="twitter:card"
          content="summary_large_image"
          key="twitter:card"
        />
        <meta
          property="twitter:title"
          content={dataContent.data.seo_title.value}
          key="twitter:title"
        />
        <meta
          property="twitter:description"
          content={dataContent.data.seo_description.value}
          key="twitter:description"
        />
        <meta
          property="twitter:image"
          content={dataContent.data.favicon.value}
          key="twitter:image"
        />

        {/* Favicon */}
        <link
          rel="icon"
          type="image/png"
          href={dataContent.data.favicon.value}
          key="favicon"
        />
      </Head>
      <h1 className="display-4 mb-5">
        {dataContent.data.news_index_title.value}
      </h1>
      <h1>{dataContent.data.news_category_title.value}</h1>
      <Row className="mb-5">
        <Col className="my-3 text-center">
          <Link href="/news?">
            <Button color="dark">All</Button>
          </Link>
        </Col>
        {news_categories.map((category) => (
          <Col key={category.id} className="my-3 text-center">
            <Link href={`/news?id_category=${category.id}`}>
              <Button color="dark">{category.name}</Button>
            </Link>
          </Col>
        ))}
      </Row>
      <Row>
        {news.map((n, index) => (
          <Col md={4} key={n.id}>
            <Card>
              <Image src={n.thumbnail} height={900} width={1600} />
              <CardBody>
                <h2>{n.title.substring(0, 20) + "..."}</h2>
                <div
                  dangerouslySetInnerHTML={{
                    __html: n.content.substring(0, 100) + "...",
                  }}
                ></div>
                <Button color="dark" block>
                  <Link href={`/news/${n.slug}`}>
                    <a className="text-white">
                      {dataContent.data.news_index_cta.value}
                    </a>
                  </Link>
                </Button>
              </CardBody>
            </Card>
          </Col>
        ))}
      </Row>
      <Row className="my-5">
        {meta.current_page > 1 ? (
          <Col md={1}>
            <Button color="dark">
              <Link
                href={`/news?id_category=${query?.id_category}&page=${
                  meta.current_page - 1
                }`}
              >
                <a className="text-white">{meta.current_page - 1}</a>
              </Link>
            </Button>
          </Col>
        ) : null}
        <Col md={1}>
          <Button color="dark" disabled>
            {meta.current_page}
          </Button>
        </Col>
        {meta.current_page < meta.last_page ? (
          <Col md={1}>
            <Button color="dark">
              <Link
                href={`/news?id_category=${query?.id_category}&page=${
                  meta.current_page + 1
                }`}
              >
                <a className="text-white">{meta.current_page + 1}</a>
              </Link>
            </Button>
          </Col>
        ) : null}
      </Row>
    </Container>
  );
}

async function prefetchAxios(key, url) {
  const result = await axios.get(url);
  return result.data;
}

export async function getServerSideProps({ query, locale }) {
  const queryCache = new QueryCache();

  let news_link = `${process.env.NEXT_PUBLIC_API_URL}/api/news?perPage=10&lang=${locale}`;
  if (query.id_category) {
    news_link += `&article_category_id=${query.id_category}`;
  } else {
    query.id_category = "";
  }
  if (query.page) {
    news_link += `&page=${query.page}`;
  }
  await queryCache.prefetchQuery(["news", news_link], prefetchAxios);

  await queryCache.prefetchQuery(
    [
      "news_categories",
      `${process.env.NEXT_PUBLIC_API_URL}/api/news_categories`,
    ],
    prefetchAxios
  );

  await queryCache.prefetchQuery(
    [
      "content-news-index",
      `${process.env.NEXT_PUBLIC_API_URL}/api/contents?keys=favicon,seo_title,seo_description,news_index_title,news_category_title,news_index_cta&lang=${locale}`,
    ],
    prefetchAxios
  );

  return {
    props: {
      query,
      dehydratedState: dehydrate(queryCache),
      locale,
    },
  };
}
