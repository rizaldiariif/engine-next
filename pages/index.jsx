import Head from "next/head";
import { QueryCache, useQuery } from "react-query";
import { Container } from "reactstrap";
import axios from "axios";
import { dehydrate } from "react-query/hydration";

export default function Index({ locale }) {
  const { data } = useQuery(
    [
      "content-main",
      `${process.env.NEXT_PUBLIC_API_URL}/api/contents?keys=favicon,seo_title,seo_description&lang=${locale}`,
    ],
    prefetchAxios
  );

  return (
    <Container className="py-4">
      <Head>
        {/* Primary Meta Tags */}
        <title>{data.data.seo_title.value}</title>
        <meta
          name="description"
          content={data.data.seo_description.value}
          key="description"
        />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" key="og:type" />
        <meta
          property="og:title"
          content={data.data.seo_title.value}
          key="og:title"
        />
        <meta
          property="og:description"
          content={data.data.seo_description.value}
          key="og:description"
        />
        <meta
          property="og:image"
          content={data.data.favicon.value}
          key="og:image"
        />

        {/* Open Graph / Facebook */}
        <meta
          property="twitter:card"
          content="summary_large_image"
          key="twitter:card"
        />
        <meta
          property="twitter:title"
          content={data.data.seo_title.value}
          key="twitter:title"
        />
        <meta
          property="twitter:description"
          content={data.data.seo_description.value}
          key="twitter:description"
        />
        <meta
          property="twitter:image"
          content={data.data.favicon.value}
          key="twitter:image"
        />

        {/* Favicon */}
        <link
          rel="icon"
          type="image/png"
          href={data.data.favicon.value}
          key="favicon"
        />
      </Head>
      <h1>Index Page</h1>
    </Container>
  );
}

async function prefetchAxios(key, url) {
  const result = await axios.get(url);
  return result.data;
}

export async function getStaticProps({ locale }) {
  const queryCache = new QueryCache();

  await queryCache.prefetchQuery(
    [
      "content-main",
      `${process.env.NEXT_PUBLIC_API_URL}/api/contents?keys=favicon,seo_title,seo_description&lang=${locale}`,
    ],
    prefetchAxios
  );

  return {
    props: { dehydratedState: dehydrate(queryCache), locale },
    revalidate: 60,
  };
}
