import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container,
} from "reactstrap";
import Link from "next/link";
import { useRouter } from "next/router";

const Header = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  const { locale, locales } = useRouter();

  return (
    <div>
      <Navbar color="light" light expand="md">
        <Container>
          <Link href="/">
            <NavbarBrand>Base Engine</NavbarBrand>
          </Link>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink>
                  <Link href="/news" className="nav-link">
                    <a className="text-dark">News</a>
                  </Link>
                </NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  {locale.toUpperCase()}
                </DropdownToggle>
                <DropdownMenu right>
                  {locales.map((l, l_i) => (
                    <Link href={`/${l}`} as={`/${l}`} key={l_i}>
                      <DropdownItem>{l.toUpperCase()}</DropdownItem>
                    </Link>
                  ))}
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default Header;
