// next.config.js
module.exports = {
  i18n: {
    locales: ['en', 'id'],
    defaultLocale: 'en',
  },
  images: {
    domains: ['engine-compro.on-dev.info'],
  },
}